﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApplication1;

namespace ParserTest
{
    /// <summary>
    /// Покрытие 4 тестами на основе https://code.google.com/codejam/contest/dashboard?c=351101#s=p2
    /// </summary>
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void hi_test()
        {
            Assert.AreEqual(Parser.Decode("hi"), "Case #1: 44 444");
        }

        [TestMethod]
        public void yes_test()
        {
            Assert.AreEqual(Parser.Decode("yes"), "Case #2: 999337777");
        }

        [TestMethod]
        public void foo__bar_test()
        {
            Assert.AreEqual(Parser.Decode("foo  bar"), "Case #3: 333666 6660 022 2777");
        }

        [TestMethod]
        public void hello_world_test()
        {
            Assert.AreEqual(Parser.Decode("hello world"), "Case #4: 4433555 555666096667775553");
        }
    }
}
