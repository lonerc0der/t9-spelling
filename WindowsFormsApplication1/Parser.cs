﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public static class Parser
    {
        // счетчик обработаных строк
        private static int processCounter = 1;
        // словарь соответствий
        private static Dictionary<char, string> matchlist = new Dictionary<char, string>()
        {
            {'a',   "2" },
            {'b',   "22" },
            {'c' ,  "222" },
            {'d',   "3" },
            {'e',   "33" },
            {'f',   "333" },
            {'g',   "4" },
            {'h',   "44" },
            {'i',   "444" },
            {'j',   "5" },
            {'k',   "55" },
            {'l',   "555" },
            {'m',   "6" },
            {'n',   "66" },
            {'o',   "666" },
            {'p',   "7" },
            {'q',   "77" },
            {'r',   "777" },
            {'s',   "7777" },
            {'t',   "8" },
            {'u',   "88" },
            {'v',   "888" },
            {'w',   "9" },
            {'x',   "99" },
            {'y',   "999" },
            {'z',   "9999" },
            {' ',   "0" },
        };

        public static string Decode(string input)
        {
            // ключ по которому будем извлекать значение из matchlist
            string key = string.Empty;
            // предыдущий символ кнопки
            char prev = ' ';
            // результирующая строка
            string result = string.Empty;
            
            for (int i = 0; i < input.Length; i++)
            {
                // получаем текущее значение по ключу
                key = matchlist[input[i]];
                // если другой символ с одной кнопки, делаем паузу
                if (i > 0 && key[0] == prev)
                    result += " ";
                result += key;
                // предыдущее значение
                prev = key[0];
            }
            return string.Format("Case #{0}: {1}", processCounter++, result);
        }
    }
}