﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// класс открытия и чтения файлов
    /// </summary>
    class FileReader
    {
        private List<string> Lines;
        public string FilePath { get; private set; }
        public FileReader()
        {
            Lines = new List<string>();
        }
        public void OpenFile()
        {
            OpenFileDialog of = new OpenFileDialog();
            if (of.ShowDialog() == DialogResult.OK)
                FilePath = of.FileName;
        }

        public List<string> ReadFile()
        {
            if (string.IsNullOrEmpty(FilePath))
                MessageBox.Show("Не выбран файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                System.IO.StreamReader file = new System.IO.StreamReader(FilePath);
                while ((file.ReadLine()) != null)
                    Lines.Add(file.ReadLine());
                return Lines;
                
            }
            return null;
        }
    }
}
