﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileReader reader = new FileReader();
            reader.OpenFile();
            if (reader.ReadFile() != null)
            {
                foreach (string str in reader.ReadFile())
                {
                    if (str != null)
                        textBox1.Text += Parser.Decode(str) + Environment.NewLine;
                }
            }
        }
    }
}
